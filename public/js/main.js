enchant();
window.onload = function () {
    var core = new Core(320, 320);
    core.preload('../images/chara1.png', '../images/icon0.png', '../images/chara6.png', '../images/gameover.png', '../images/map0.png');
    core.fps = 15;
    core.onload = function () {
        var kuma = new Kuma(core);
        var tekiController = new TekiController(core);
        var gamecontroller = new GameController(core, kuma, tekiController);
    };
    core.start();
};
$(function () {
});
var TekiController = (function () {
    function TekiController(core) {
        this.core = core;
        this.tekiCount = 0;
        this.tekiMax = 10;
        this.tekiGroup = new Group();
        this.teki = new Array(this.tekiMax);
        for(var i = 0; i < this.tekiMax; i++) {
            this.teki[i] = new Teki(this.core, this.tekiGroup, {
                x: Math.floor(Math.random() * 300),
                y: Math.floor(Math.random() * 200)
            });
            this.tekiCount++;
        }
        console.log("tekiCount:" + this.tekiCount);
    }
    TekiController.prototype.atari = function (kuma, gc) {
        for(var i = 0; i < this.teki.length; i++) {
            if(this.teki[i] != undefined) {
                if(this.teki[i].tekiSprite.intersect(kuma.kumasprite)) {
                    this.getTekiNum(i, gc);
                    console.log("残り：" + this.tekiCount);
                }
            }
        }
        if(this.tekiCount <= 0) {
            gc.over();
        }
    };
    TekiController.prototype.getTekiNum = function (target, gc) {
        console.log("getTekiNum:" + target);
        this.tekiDelete(target);
        gc.serverGetTeki(target);
        gc.pointup();
    };
    TekiController.prototype.tekiDelete = function (target) {
        console.log("tekiDelete:" + target);
        this.tekiGroup.removeChild(this.teki[target].tekiSprite);
        delete (this.teki[target]);
        this.tekiCount--;
    };
    TekiController.prototype.getTekiPosi = function () {
        var tekiPosi = new Array(this.tekiMax);
        for(var i = 0; i < this.tekiMax; i++) {
            tekiPosi[i] = {
                x: this.teki[i].tekiSprite.x,
                y: this.teki[i].tekiSprite.y
            };
        }
        return tekiPosi;
    };
    TekiController.prototype.setTekiPosi = function (tekiPosi) {
        for(var i = 0; i < this.tekiMax; i++) {
            console.log("setTekiPosi: " + i + ":" + tekiPosi[i].x + "x" + tekiPosi[i].y);
            this.teki[i].tekiSprite.x = tekiPosi[i].x;
            this.teki[i].tekiSprite.y = tekiPosi[i].y;
        }
    };
    return TekiController;
})();
var Teki = (function () {
    function Teki(core, tg, tekiposi) {
        this.core = core;
        this.tg = tg;
        this.tekiSprite = new Sprite(32, 32);
        this.tekiSprite.image = this.core.assets['../images/chara6.png'];
        this.tekiSprite.frame = [
            3, 
            4, 
            5
        ];
        this.setPosi(tekiposi);
        tg.addChild(this.tekiSprite);
    }
    Teki.prototype.setPosi = function (tekiposi) {
        this.tekiSprite.x = tekiposi.x;
        this.tekiSprite.y = tekiposi.y;
    };
    return Teki;
})();
var Kuma = (function () {
    function Kuma(core) {
        this.core = core;
        this.speed = 5;
        this.kumasprite = new Sprite(32, 32);
        this.kumasprite.image = core.assets['../images/chara1.png'];
        this.kumasprite.frame = [
            0, 
            1, 
            2
        ];
        this.kumasprite.x = Math.floor(Math.random() * 300);
        this.kumasprite.y = Math.floor(Math.random() * 200);
        console.log("my kuma x:" + this.kumasprite.x + " y: " + this.kumasprite.y);
    }
    Kuma.prototype.start = function () {
        var _this = this;
        this.kumasprite.addEventListener('enterframe', function () {
            if(_this.goalX - _this.kumasprite.x > 0) {
                _this.kumasprite.x += _this.speed;
            }
            if(_this.goalX - _this.kumasprite.x < 0) {
                _this.kumasprite.x -= _this.speed;
            }
            if(_this.goalY - _this.kumasprite.y > 0) {
                _this.kumasprite.y += _this.speed;
            }
            if(_this.goalY - _this.kumasprite.y < 0) {
                _this.kumasprite.y -= _this.speed;
            }
        });
        this.core.rootScene.addEventListener('touchstart', function (e) {
            console.log("X:" + e.localX + " Y:" + e.localY);
            _this.goalX = e.localX;
            _this.goalY = e.localY;
        });
    };
    return Kuma;
})();
var GameController = (function () {
    function GameController(core, kuma, tc) {
        this.core = core;
        this.kuma = kuma;
        this.tc = tc;
        this.LIMIT_TIME = 15;
        this.resttime = 0;
        this.mycount = 0;
        var map = new Map(16, 16);
        map.image = core.assets['../images/map0.png'];
        map.loadData([
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ], 
            [
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0, 
                0
            ]
        ]);
        core.rootScene.addChild(map);
        core.rootScene.addChild(kuma.kumasprite);
        core.rootScene.addChild(tc.tekiGroup);
        console.log("map");
        this.waiting();
        this.serverconnect(this.kuma.kumasprite.x, this.kuma.kumasprite.y);
    }
    GameController.prototype.waiting = function () {
        this.waitinglabel = new Label();
        this.waitinglabel.x = 100;
        this.waitinglabel.y = 200;
        this.waitinglabel.text = "あいてをまっています<br>じゅんびしててね";
        this.core.rootScene.addChild(this.waitinglabel);
    };
    GameController.prototype.timelabel = function () {
        var _this = this;
        this.startframe = this.core.frame;
        this.resttime = this.LIMIT_TIME;
        this.timeLabel = new Label();
        this.timeLabel.x = 0;
        this.timeLabel.y = 0;
        this.timeLabel.text = "残り時間: " + this.resttime;
        this.core.rootScene.addChild(this.timeLabel);
        this.timeLabel.addEventListener('enterframe', function () {
            _this.resttime = _this.LIMIT_TIME - (_this.core.frame - _this.startframe) / _this.core.fps;
            if(_this.resttime <= 0) {
                _this.over();
            } else {
                _this.timeLabel.text = "残り時間: " + Math.floor(_this.resttime);
            }
        });
    };
    GameController.prototype.start = function () {
        var _this = this;
        this.core.rootScene.removeChild(this.waitinglabel);
        this.kuma.start();
        this.timelabel();
        this.core.rootScene.addEventListener('enterframe', function () {
            _this.servermove(_this.kuma.kumasprite.x, _this.kuma.kumasprite.y);
            _this.tc.atari(_this.kuma, _this);
        });
    };
    GameController.prototype.pointup = function () {
        this.mycount++;
    };
    GameController.prototype.over = function () {
        this.timeLabel.clearEventListener('enterframe');
        this.lastScene = new Scene();
        this.lastScene.backgroundColor = 'rgba(0,0,0,0.5)';
        this.core.replaceScene(this.lastScene);
        var gameoverlabel = Label();
        gameoverlabel.text = "You Get " + this.mycount;
        gameoverlabel.x = 80;
        gameoverlabel.y = 200;
        this.lastScene.addChild(gameoverlabel);
        var gameover = new Sprite(189, 97);
        gameover.image = this.core.assets['../images/gameover.png'];
        gameover.frame = [
            1
        ];
        gameover.x = 60;
        gameover.y = 30;
        this.lastScene.addChild(gameover);
        this.serverFinish(this.mycount);
    };
    GameController.prototype.finish = function (score) {
        var finishlabel = Label();
        if(this.mycount == score) {
            finishlabel.text = "あなたのかち:" + this.mycount;
        } else {
            finishlabel.text = "あなたのまけ:" + this.mycount;
        }
        finishlabel.x = 100;
        finishlabel.y = 220;
        this.lastScene.addChild(finishlabel);
        this.core.stop();
    };
    GameController.prototype.serverconnect = function (kumax, kumay) {
        var _this = this;
        this.socket = io.connect("/");
        this.socket.on("connect", function () {
            console.log("connect=>waiting" + kumax + "x" + kumay);
            _this.socket.emit("waiting", {
                x: kumax,
                y: kumay
            }, _this.tc.getTekiPosi());
        });
        this.socket.on("ready", function (kuma, tekiPosi) {
            console.log("ready: " + kuma.x + "x" + kuma.y);
            _this.tc.setTekiPosi(tekiPosi);
            if(_this.shirokuma != null) {
                _this.shirokuma.clear();
            }
            _this.shirokuma = new ShiroKuma(_this.core, kuma.x, kuma.y);
            _this.start();
        });
        this.socket.on("kumamove", function (kuma) {
            if(_this.shirokuma != undefined) {
                _this.shirokuma.move(kuma.x, kuma.y);
            }
        });
        this.socket.on("getteki", function (target) {
            console.log("getteki:" + target);
            _this.tc.tekiDelete(target);
        });
        this.socket.on("disconnect", function () {
        });
        this.socket.on("result", function (highScore) {
            console.log("result:" + highScore);
            _this.finish(highScore);
        });
    };
    GameController.prototype.servermove = function (x, y) {
        this.socket.emit("kumamove", {
            x: x,
            y: y
        });
    };
    GameController.prototype.serverGetTeki = function (target) {
        this.socket.emit("getteki", target);
    };
    GameController.prototype.serverFinish = function (myscore) {
        this.socket.emit("finish", myscore);
    };
    return GameController;
})();
var ShiroKuma = (function () {
    function ShiroKuma(core, kumax, kumay) {
        this.core = core;
        this.sprite = new Sprite(32, 32);
        this.sprite.image = core.assets['../images/chara1.png'];
        this.sprite.frame = [
            5, 
            6, 
            7
        ];
        this.sprite.x = kumax;
        this.sprite.y = kumay;
        this.core.rootScene.addChild(this.sprite);
    }
    ShiroKuma.prototype.move = function (kumax, kumay) {
        this.sprite.x = kumax;
        this.sprite.y = kumay;
    };
    ShiroKuma.prototype.clear = function () {
        this.core.rootScene.removeChild(this.sprite);
        console.log("clear");
    };
    return ShiroKuma;
})();
//@ sourceMappingURL=main.js.map
