///<reference path='../../d.ts/enchant.d.ts' />
///<reference path='../../d.ts/DefinitelyTyped/jquery/jquery.d.ts' />
///<reference path='../../d.ts/DefinitelyTyped/socket.io/socket.io.d.ts' />

declare var Core;
declare var Sprite;
declare var Scene;
declare var enchant;
declare var Pad;
declare var Button;
declare var core;
declare var socket;
declare var io;
declare var Label;
declare var Map;
declare var Group;

enchant();

window.onload = function() {

    var core = new Core(320, 320);
    core.preload('../images/chara1.png','../images/icon0.png','../images/chara6.png','../images/gameover.png','../images/map0.png');
    core.fps = 15;

    core.onload = function() {
        var kuma = new Kuma(core);
	var tekiController = new TekiController(core);
	var gamecontroller = new GameController(core, kuma,tekiController);

//	kuma.start();
//	gamecontroller.start();
    };
    core.start();
};

$(function() {

});

class TekiController {

    teki:Teki[];
    tekiCount:number = 0;
    tekiMax:number = 10;
    tekiGroup:any;

    constructor(private core:any) {
	this.tekiGroup = new Group();

	this.teki = new Array(this.tekiMax);
        for(var i:number=0;i<this.tekiMax;i++) {
            this.teki[i] = new Teki(this.core, this.tekiGroup, {x: Math.floor(Math.random() * 300), y: Math.floor(Math.random()* 200)});
	    this.tekiCount++;
        }
	console.log("tekiCount:" + this.tekiCount);
    }

    atari(kuma:Kuma, gc:GameController) {
	for(var i=0;i< this.teki.length; i++ ) {
	    if(this.teki[i] != undefined ) {
		if(this.teki[i].tekiSprite.intersect(kuma.kumasprite)) {
		    this.getTekiNum(i,gc);
		    console.log("残り：" + this.tekiCount);
		}
	    }
	}
	if(this.tekiCount <= 0) {
	    gc.over();
	}
    }
    getTekiNum(target:number, gc:GameController) {
	console.log("getTekiNum:" + target);
	this.tekiDelete(target);

	gc.serverGetTeki(target);
	gc.pointup();
    }
    tekiDelete(target:number){
	console.log("tekiDelete:"+target);
	this.tekiGroup.removeChild(this.teki[target].tekiSprite);
	delete(this.teki[target]);
	this.tekiCount--;
    }

    getTekiPosi() {
	var tekiPosi:Posi[] = new Array(this.tekiMax);
	for(var i=0;i<this.tekiMax;i++) {
	    tekiPosi[i] = {x: this.teki[i].tekiSprite.x, y: this.teki[i].tekiSprite.y};
	}

	return tekiPosi;
    }
    setTekiPosi(tekiPosi:Posi[]) {
	for(var i=0;i<this.tekiMax;i++) {
	    console.log("setTekiPosi: " + i + ":" + tekiPosi[i].x + "x" + tekiPosi[i].y);
	    this.teki[i].tekiSprite.x = tekiPosi[i].x;
	    this.teki[i].tekiSprite.y = tekiPosi[i].y;
	}
    }	
}

class Teki {

    tekiSprite:any;
    
    constructor(private core:any, private tg:any, tekiposi:Posi) {
        this.tekiSprite = new Sprite(32,32);
        this.tekiSprite.image = this.core.assets['../images/chara6.png'];
        this.tekiSprite.frame = [3,4,5];
	this.setPosi(tekiposi);
        tg.addChild(this.tekiSprite);
    }

    setPosi(tekiposi:Posi) {
        this.tekiSprite.x = tekiposi.x;
        this.tekiSprite.y = tekiposi.y;
    }
    
}

class Kuma {
    kumasprite: any;
    private speed:number = 5;
    private goalX:number;
    private goalY:number;
    
    constructor(private core:any) {
        this.kumasprite = new Sprite(32, 32);
        this.kumasprite.image = core.assets['../images/chara1.png'];
        this.kumasprite.frame = [0,1,2];
        this.kumasprite.x = Math.floor(Math.random() * 300);
        this.kumasprite.y = Math.floor(Math.random() * 200);

	console.log("my kuma x:" + this.kumasprite.x + " y: "+this.kumasprite.y);
        
    }

    start() {
	this.kumasprite.addEventListener('enterframe', () => {
            if (this.goalX - this.kumasprite.x > 0 ) this.kumasprite.x += this.speed;
            if (this.goalX - this.kumasprite.x < 0 ) this.kumasprite.x -= this.speed;
            if (this.goalY - this.kumasprite.y > 0 ) this.kumasprite.y += this.speed;
            if (this.goalY - this.kumasprite.y < 0 ) this.kumasprite.y -= this.speed;
        });
	
        this.core.rootScene.addEventListener('touchstart', (e) => {
	    console.log("X:" + e.localX + " Y:" + e.localY);
	    this.goalX = e.localX;
	    this.goalY = e.localY;
        });
    }
}

class GameController {
    timeLabel:any;
    LIMIT_TIME:number = 15;
    resttime:number = 0;
    private mycount:number = 0;
    private socket:Socket;
    private shirokuma:ShiroKuma;
    private waitinglabel:any;
    private startframe:number;
    private lastScene:any;

    constructor(private core:any, private kuma:Kuma, private tc:TekiController) {
	var map = new Map(16,16);
	map.image = core.assets['../images/map0.png'];
	map.loadData([
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
	    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
	]);
	core.rootScene.addChild(map);
	core.rootScene.addChild(kuma.kumasprite);
	core.rootScene.addChild(tc.tekiGroup);
	console.log("map");
	this.waiting();

	this.serverconnect(this.kuma.kumasprite.x, this.kuma.kumasprite.y);
    }

    waiting() {
	this.waitinglabel = new Label();
	this.waitinglabel.x = 100;
	this.waitinglabel.y = 200;
	this.waitinglabel.text = "あいてをまっています<br>じゅんびしててね";
//	waitinglabel.font = "30px";

	this.core.rootScene.addChild(this.waitinglabel);
    }

    timelabel() {
	this.startframe = this.core.frame;
	this.resttime = this.LIMIT_TIME;
	this.timeLabel = new Label();
	this.timeLabel.x = 0;
	this.timeLabel.y = 0;
	this.timeLabel.text = "残り時間: " + this.resttime;

	this.core.rootScene.addChild(this.timeLabel);

	this.timeLabel.addEventListener('enterframe', () => {
	    this.resttime = this.LIMIT_TIME - (this.core.frame - this.startframe)/this.core.fps;
	    if(this.resttime <= 0) {
		this.over();
	    } else {
		this.timeLabel.text = "残り時間: " + Math.floor(this.resttime);
	    }
	});
    }	

    start() {
	this.core.rootScene.removeChild(this.waitinglabel);
	this.kuma.start();
	this.timelabel();

	this.core.rootScene.addEventListener('enterframe', () => {
	    this.servermove(this.kuma.kumasprite.x, this.kuma.kumasprite.y);
	    this.tc.atari(this.kuma, this);
	});
    }

    public pointup() {
	this.mycount++;

    }

    over() {
	this.timeLabel.clearEventListener('enterframe');
	this.lastScene = new Scene();
	this.lastScene.backgroundColor = 'rgba(0,0,0,0.5)';
	this.core.replaceScene(this.lastScene);

	var gameoverlabel = Label();
	gameoverlabel.text = "You Get " + this.mycount;
	gameoverlabel.x = 80;
	gameoverlabel.y = 200;	
	this.lastScene.addChild(gameoverlabel);

        var gameover = new Sprite(189,97);
        gameover.image = this.core.assets['../images/gameover.png'];
        gameover.frame = [1];
	gameover.x = 60;
	gameover.y = 30;

	this.lastScene.addChild(gameover);

	this.serverFinish(this.mycount);
    }

    finish(score:number) {
	var finishlabel = Label();
	if(this.mycount == score) {
	    finishlabel.text = "あなたのかち:" + this.mycount;
	} else {
	    finishlabel.text = "あなたのまけ:" + this.mycount;	    
	}
	finishlabel.x = 100;
	finishlabel.y = 220;

	this.lastScene.addChild(finishlabel);
	this.core.stop();
    }

    serverconnect(kumax:number, kumay:number) {
	//    var socket = io.connect('http://localhost:3000');
//	this.socket = io.connect("/",{port: 3000});
	this.socket = io.connect("/");
	// 接続したら
	this.socket.on("connect", () => {
	    console.log("connect=>waiting"+kumax+"x"+kumay);
	    this.socket.emit("waiting", {x: kumax, y: kumay}, this.tc.getTekiPosi());
	});
	
	this.socket.on("ready", (kuma:Posi, tekiPosi:Posi[]) => {
	    console.log("ready: " + kuma.x + "x" + kuma.y);

	    this.tc.setTekiPosi(tekiPosi);

	    if(this.shirokuma != null) {
		this.shirokuma.clear();
	    }
	    this.shirokuma = new ShiroKuma(this.core, kuma.x, kuma.y);
	    this.start();
	});

	this.socket.on("kumamove", (kuma) => {
//	    console.log("kumamove" + kuma.x + "x" + kuma.y);
	    if(this.shirokuma != undefined ) {
		this.shirokuma.move(kuma.x,kuma.y);
	    }
	});

	this.socket.on("getteki", (target:number) => {
	    console.log("getteki:" + target);
	    this.tc.tekiDelete(target);
	});

	this.socket.on("disconnect", () => {
//	    this.shirokuma.clear();
	});

	this.socket.on("result", (highScore:number) => {
	    console.log("result:" + highScore);
	    this.finish(highScore);
	});
    }

    servermove(x:number, y:number) {
	this.socket.emit("kumamove", {x: x, y: y});
    }
    
    serverGetTeki(target:number) {
	this.socket.emit("getteki", target);
    }

    serverFinish(myscore:number) {
	this.socket.emit("finish", myscore);
    }
}

class ShiroKuma {
    private sprite: any;
    constructor(private core:any, kumax:number, kumay:number){
	this.sprite = new Sprite(32,32);
	this.sprite.image = core.assets['../images/chara1.png'];
	this.sprite.frame = [5,6,7];
	this.sprite.x = kumax;
	this.sprite.y = kumay;

	this.core.rootScene.addChild(this.sprite);
    }

    move(kumax:number, kumay:number) {
	this.sprite.x = kumax;
	this.sprite.y = kumay;
    }

    clear() {
	this.core.rootScene.removeChild(this.sprite);
	console.log("clear");
    }
}

interface Posi {
    x:number;
    y:number;
}
