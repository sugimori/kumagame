module.exports = function(grunt) {

    grunt.loadNpmTasks('grunt-typescript');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Project configuration.
    grunt.initConfig({
	pkg: grunt.file.readJSON('package.json'),
	typescript: {
	    client: {
		src: ['public/ts/*.ts'],
		dest: 'public/js/',
		options: {
		    target: 'es5', //or es3
		    sourcemap: true,
		    base_path: 'public/ts'
		}
	    },
	    app: {
		src: ['app.ts'],
		dest: 'app.js',
		options: {
		    target: 'es5',
		    sourcemap: true,
		}
	    },
	    routes: {
		src: ['routes/*.ts'],
		dest: 'routes/',
		options: {
		    target: 'es5',
		    sourcemap: true,
		    base_path: 'routes/'
		}
	    }
	},
        watch: {
            files: ['*.ts','routes/*.ts','public/ts/*.ts'],
            tasks: ['typescript']
        }
    });
    grunt.registerTask('default', 'watch');
};
