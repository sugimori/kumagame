///<reference path='d.ts/DefinitelyTyped/node/node.d.ts' />
///<reference path='d.ts/DefinitelyTyped/express/express.d.ts' />
///<reference path='d.ts/DefinitelyTyped/socket.io/socket.io.d.ts' />

/**
 * Module dependencies.
 */

var express = require('express');

var routes = require('./routes')
, user = require('./routes/user')
, http = require('http');

var path = require('path');

var app = express();

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
//   app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

// app.get('/', routes.index);
// app.get('/users', user.list);

var server = http.createServer(app);
server.listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});

var io = require('socket.io').listen(server);

io.set('log level', 2);

class ShareController {
    user1:userClient = null;
    user2:userClient = null;
    tekiPosi:Posi[];
    finish1:bool = false;
    finish2:bool = false;
    highScore:number = 0;
//    tekiMax:number = 10;

//    tekiPosi:tekiPosi[];
    
    constructor(private io:SocketManager) {
	console.log("ShareController constructor");
//	this.tekiPosi = new Array(this.tekiMax);
	this.watch();
	io.sockets.on("connection", (socket) => {
	    console.log("connect from new client");
	    socket.on("waiting", (kuma:Posi, tekiPosi:Posi[]) => {
		console.log("waiting: " + kuma.x + "x" + kuma.y);
		this.watch();
		this.tekiPosi = tekiPosi;
		console.log("tekiposi: " + tekiPosi[0].x);
		var newuser = new userClient(socket,kuma);
		if(this.user1 == null ) {
		    this.user1 = newuser;
		}else if(this.user2 == null) {
		    this.user2 = newuser;
		} else {
		    this.user1 = newuser;
		    this.user2 = null;
		    console.log("Ocupayed");
		}
		this.watch();

		if((this.user1 != null) && (this.user2 != null)) {
		    this.user1.socket.emit("ready",this.user2.kuma, this.tekiPosi);
		    this.user2.socket.emit("ready",this.user1.kuma, this.tekiPosi);
		    this.finish1 = false;
		    this.finish2 = false;
		    this.highScore = 0;
		}
		this.watch();
	    });

	    socket.on("disconnect", () => {
	    });

	    socket.on("kumamove", (kuma) => {
//		console.log("kumamove(server) x:"+kuma.x + " y: " + kuma.y);
		socket.broadcast.volatile.emit("kumamove", kuma);
	    });

	    socket.on("getteki", (target:number) => {
		console.log("getteki:" + target);
		socket.broadcast.emit("getteki", target);
	    });

	    socket.on("finish", (score:number) => {
		console.log("finish:" + score);
		this.highScore = Math.max(this.highScore, score);
		if(this.finish1 == false) {
		    this.finish1 = true;
		} else if (this.finish2 == false ) {
		    this.finish2 = true;
		}
		if(this.finish1 && this.finish2) {
		    this.io.sockets.emit("result", this.highScore);
		}
		
	    });

	});
	
    }

    watch() {
	    if(this.user1 == null) {
		console.log("user1: null");
	    } else {
		console.log("user1:" + this.user1.kuma.x + "x" + this.user1.kuma.y)
	    }
	    if(this.user2 == null) {
		console.log("user2: null");		
	    } else {
		console.log("user2:" + this.user2.kuma.x + "x" + this.user2.kuma.y)
	    }
    }
}

class userClient {
    constructor(public socket:Socket, public kuma:Posi) {
    }
}

interface Posi {
    x:number;
    y:number;
}
	
var sc = new ShareController(io);


