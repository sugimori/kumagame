var express = require('express');
var routes = require('./routes'), user = require('./routes/user'), http = require('http');
var path = require('path');
var app = express();
app.configure(function () {
    app.set('port', process.env.PORT || 3000);
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(express.favicon());
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(app.router);
    app.use(express.static(path.join(__dirname, 'public')));
});
app.configure('development', function () {
    app.use(express.errorHandler());
});
var server = http.createServer(app);
server.listen(app.get('port'), function () {
    console.log("Express server listening on port " + app.get('port'));
});
var io = require('socket.io').listen(server);
io.set('log level', 2);
var ShareController = (function () {
    function ShareController(io) {
        this.io = io;
        var _this = this;
        this.user1 = null;
        this.user2 = null;
        this.finish1 = false;
        this.finish2 = false;
        this.highScore = 0;
        console.log("ShareController constructor");
        this.watch();
        io.sockets.on("connection", function (socket) {
            console.log("connect from new client");
            socket.on("waiting", function (kuma, tekiPosi) {
                console.log("waiting: " + kuma.x + "x" + kuma.y);
                _this.watch();
                _this.tekiPosi = tekiPosi;
                console.log("tekiposi: " + tekiPosi[0].x);
                var newuser = new userClient(socket, kuma);
                if(_this.user1 == null) {
                    _this.user1 = newuser;
                } else if(_this.user2 == null) {
                    _this.user2 = newuser;
                } else {
                    _this.user1 = newuser;
                    _this.user2 = null;
                    console.log("Ocupayed");
                }
                _this.watch();
                if((_this.user1 != null) && (_this.user2 != null)) {
                    _this.user1.socket.emit("ready", _this.user2.kuma, _this.tekiPosi);
                    _this.user2.socket.emit("ready", _this.user1.kuma, _this.tekiPosi);
                    _this.finish1 = false;
                    _this.finish2 = false;
                    _this.highScore = 0;
                }
                _this.watch();
            });
            socket.on("disconnect", function () {
            });
            socket.on("kumamove", function (kuma) {
                socket.broadcast.volatile.emit("kumamove", kuma);
            });
            socket.on("getteki", function (target) {
                console.log("getteki:" + target);
                socket.broadcast.emit("getteki", target);
            });
            socket.on("finish", function (score) {
                console.log("finish:" + score);
                _this.highScore = Math.max(_this.highScore, score);
                if(_this.finish1 == false) {
                    _this.finish1 = true;
                } else if(_this.finish2 == false) {
                    _this.finish2 = true;
                }
                if(_this.finish1 && _this.finish2) {
                    _this.io.sockets.emit("result", _this.highScore);
                }
            });
        });
    }
    ShareController.prototype.watch = function () {
        if(this.user1 == null) {
            console.log("user1: null");
        } else {
            console.log("user1:" + this.user1.kuma.x + "x" + this.user1.kuma.y);
        }
        if(this.user2 == null) {
            console.log("user2: null");
        } else {
            console.log("user2:" + this.user2.kuma.x + "x" + this.user2.kuma.y);
        }
    };
    return ShareController;
})();
var userClient = (function () {
    function userClient(socket, kuma) {
        this.socket = socket;
        this.kuma = kuma;
    }
    return userClient;
})();
var sc = new ShareController(io);
//@ sourceMappingURL=app.js.map
